*** Life<Cell> 9x2 ***

Generation = 0, Population = 11.
00
-0
-0
--
00
--
0-
00
00

Generation = 1, Population = 9.
1-
--
01
0-
11
-0
1-
1-
--

Generation = 2, Population = 10.
-0
-0
-*
-0
--
01
*-
*0
0-

Generation = 3, Population = 13.
11
0-
0*
01
1-
-*
.0
*1
1-

*** Life<Cell> 7x5 ***

Generation = 0, Population = 15.
-0--0
-00-0
000--
---0-
00---
00---
-00--

Generation = 1, Population = 15.
01-01
-1--1
11--0
----0
--00-
-1---
--10-

Generation = 2, Population = 18.
1--1-
0-0--
*-00-
000-1
--11-
0-0--
--*10

*** Life<Cell> 8x6 ***

Generation = 0, Population = 25.
0--0-0
0-00--
0-000-
00-000
0--00-
00--0-
-----0
0-0--0

Generation = 1, Population = 23.
10-1--
---1-0
-0----
1101-1
-00-1-
-10-1-
-00--1
---001

Generation = 2, Population = 26.
*1-*00
0-0*--
-1----
*-1*0-
-110*-
0*1-*-
0---0*
-0-1--

Generation = 3, Population = 23.
**0*-1
1-1*-0
0-0-0-
*--.-1
0--1.0
-.-0.-
100--*
-----1

*** Life<Cell> 6x5 ***

Generation = 0, Population = 16.
0--00
-000-
0--00
0-00-
--0--
-0-00

Generation = 1, Population = 18.
----1
01-10
1-011
1----
0-100
0-011

Generation = 2, Population = 13.
00--*
-*0-1
-01*-
-0---
---1-
1--*-

Generation = 3, Population = 18.
1--0.
-.-1*
11**-
11---
001*0
-00.1

Generation = 4, Population = 14.
-001*
-.-**
-*.*-
**0-0
--*.-
---.*

Generation = 5, Population = 12.
11-**
-*-..
-*..1
*.-0-
0-*.-
--0..

*** Life<Cell> 3x1 ***

Generation = 0, Population = 2.
-
0
0

Generation = 1, Population = 3.
0
1
1

Generation = 2, Population = 2.
1
-
*

Generation = 3, Population = 0.
-
-
.

Generation = 4, Population = 0.
-
-
.

*** Life<Cell> 1x5 ***

Generation = 0, Population = 2.
--0-0

Generation = 1, Population = 1.
-0---

Generation = 2, Population = 2.
0-0--

Generation = 3, Population = 1.
---0-

Generation = 4, Population = 2.
--0-0

Generation = 5, Population = 1.
-0---

Generation = 6, Population = 2.
0-0--

Generation = 7, Population = 1.
---0-

Generation = 8, Population = 2.
--0-0

*** Life<Cell> 1x8 ***

Generation = 0, Population = 2.
00------

Generation = 1, Population = 3.
110-----

Generation = 2, Population = 3.
*-10----

*** Life<Cell> 10x3 ***

Generation = 0, Population = 14.
--0
0--
000
0--
0-0
--0
---
0-0
-0-
0-0

Generation = 1, Population = 15.
00-
1--
1-1
---
1-1
001
0--
-0-
0-0
-0-

Generation = 2, Population = 12.
-10
--0
*--
---
*0*
1--
100
0--
---
---

Generation = 3, Population = 15.
0*-
1-1
.01
-00
*-.
-0-
*-1
1-0
0--
---

Generation = 4, Population = 17.
-*-
*-*
*-*
---
*0*
1--
*0*
--1
100
0--

Generation = 5, Population = 12.
-*-
*0*
.-.
-0-
*-.
-0-
*-*
-0-
---
1-0

Generation = 6, Population = 15.
-*-
*1*
*-*
--0
*0.
1--
*-*
---
100
---

Generation = 7, Population = 17.
-*-
***
*0*
--1
*1.
--1
.-.
-0-
*-1
100

Generation = 8, Population = 11.
-*-
...
*1.
-0*
.**
1--
.0.
---
*-*
---

*** Life<Cell> 9x10 ***

Generation = 0, Population = 52.
-000---000
0--0-00-0-
000--00-00
-0-00-00-0
00--0-00-0
--0--0000-
---000---0
000000--00
000-0----0

Generation = 1, Population = 55.
-1--00-111
1001---0-0
-110--10--
0-01-01-0-
1---101101
0---0-1110
000-11-001
-111--0011
-1-01----1

Generation = 2, Population = 44.
-*--11----
*-1--0--01
0--10----0
10--0-*-1-
-00--1-*--
--00-----1
-1-0-*-1-*
-**-0-11**
0*0-*000-*

Generation = 3, Population = 45.
-*-0*-0-11
*0--01001-
---*--1-01
----10*0--
------1*-1
-0--0---1*
0*-11*0*0*
-..11---.*
1.--*-1-0*

Generation = 4, Population = 38.
-*-1*111-*
*-11----*1
011*00---*
----*-.---
-0-----.0*
-10-1--1-.
1*---.-*1.
-**-*--1..
-.--*---1*

Generation = 5, Population = 45.
-*0-*--*1*
*--*0-0-.*
--*.111--*
1-0-*-.-1-
11-----.1.
-*1--01*1*
*.0--.0**.
-**-.01*.*
-.00.0--*.

Generation = 6, Population = 44.
-*11*1-*-*
.-1.111-.*
-1*.*--0-*
*01-*-.0*-
--0-111.-.
0.-011*.-*
*.-1-.1...
-..-*-*..*
-*-1*1--*.

Generation = 7, Population = 45.
0**-.-1.-.
.0-.***0.*
--..*--10*
*1*-*-.-*-
--10**-.1.
1*--**.*1.
..0*1.-*..
0.*1.0**..
1.-***-0..

*** Life<Cell> 9x3 ***

Generation = 0, Population = 14.
00-
-0-
0-0
00-
-0-
--0
--0
-00
00-

Generation = 1, Population = 8.
1-0
01-
1--
---
-1-
--1
---
---
1--

Generation = 2, Population = 11.
*0-
1*-
*--
00-
0--
---
--0
0--
-0-

Generation = 3, Population = 15.
*-0
*.0
.0-
110
1--
0-1
00-
--0
--0

Generation = 4, Population = 14.
.-1
*.1
.10
-*1
---
-0-
-10
0-1
-01

Generation = 5, Population = 10.
.0*
..-
*-1
1*-
--0
01-
---
---
-1-

Generation = 6, Population = 12.
.1.
..-
*1-
-.1
-1-
1*-
01-
-0-
1-1

Generation = 7, Population = 11.
.-.
**-
.*-
1.-
-*-
-.1
-*0
01-
-1-

Generation = 8, Population = 13.
.1.
**1
.*1
-*-
-*-
-.*
-.-
1*-
-*1

*** Life<Cell> 8x9 ***

Generation = 0, Population = 42.
00-00----
00-00-000
00000-00-
0-0--0-00
00000----
-0---0--0
--0-0--00
0-000000-

Generation = 1, Population = 39.
-----0000
11011--11
1111-0-10
---00-0-1
-11-10-0-
-1000-0-1
0-1010---
----1----

*** Life<Cell> 10x8 ***

Generation = 0, Population = 39.
--000-00
-00-00-0
-00-----
0----00-
00-0000-
--00-0--
-0000--0
-----0--
-0-0-0--
00-0-000

Generation = 1, Population = 42.
-----01-
0-10-101
---00-00
1000---0
-10----0
00-1-1-0
0111100-
--0--100
-1-1---0
1--1---1

Generation = 2, Population = 44.
0-000---
--*-0---
-00110-1
*11-----
0*-0-00-
-1-*0*01
-***---0
0010--1-
--0*00--
--0*0-0*

Generation = 3, Population = 29.
-----0--
0-.----1
----*1--
.--00-00
-.010---
--0.-.1-
-...-001
11-1----
0-1.1100
-0-.-01.

*** Life<Cell> 5x1 ***

Generation = 0, Population = 2.
-
-
-
0
0

Generation = 1, Population = 3.
-
-
0
1
1

Generation = 2, Population = 3.
-
0
1
-
*

Generation = 3, Population = 3.
0
1
*
-
.

Generation = 4, Population = 2.
1
-
.
1
.

Generation = 5, Population = 1.
-
1
.
-
.

Generation = 6, Population = 1.
1
-
.
-
.

*** Life<Cell> 5x1 ***

Generation = 0, Population = 3.
-
0
0
0
-

Generation = 1, Population = 4.
0
1
-
1
0

Generation = 2, Population = 4.
1
*
-
*
1

Generation = 3, Population = 2.
*
.
-
.
*

Generation = 4, Population = 0.
.
.
-
.
.

Generation = 5, Population = 0.
.
.
-
.
.

*** Life<Cell> 4x1 ***

Generation = 0, Population = 2.
-
0
-
0

Generation = 1, Population = 1.
0
-
-
-

Generation = 2, Population = 1.
-
0
-
-

Generation = 3, Population = 2.
0
-
0
-

Generation = 4, Population = 1.
-
-
-
0

Generation = 5, Population = 1.
-
-
0
-

Generation = 6, Population = 2.
-
0
-
0

Generation = 7, Population = 1.
0
-
-
-

Generation = 8, Population = 1.
-
0
-
-

Generation = 9, Population = 2.
0
-
0
-

Generation = 10, Population = 1.
-
-
-
0

*** Life<Cell> 3x3 ***

Generation = 0, Population = 2.
---
0-0
---

Generation = 1, Population = 4.
0-0
---
0-0

Generation = 2, Population = 0.
---
---
---

Generation = 3, Population = 0.
---
---
---

Generation = 4, Population = 0.
---
---
---

Generation = 5, Population = 0.
---
---
---

Generation = 6, Population = 0.
---
---
---

Generation = 7, Population = 0.
---
---
---

Generation = 8, Population = 0.
---
---
---

Generation = 9, Population = 0.
---
---
---

Generation = 10, Population = 0.
---
---
---

*** Life<Cell> 6x4 ***

Generation = 0, Population = 14.
00-0
--0-
-0-0
0-00
---0
0000

Generation = 1, Population = 13.
110-
00-0
---1
-011
-00-
1---

Generation = 2, Population = 8.
-*1-
--01
0---
0-*-
----
--0-

Generation = 3, Population = 12.
1*--
0--*
10-1
1-.1
0---
-0-0

Generation = 4, Population = 10.
-*10
-00.
*1--
--**
1---
----

Generation = 5, Population = 14.
1**1
-1-*
*-01
--*.
-000
1---

Generation = 6, Population = 11.
**.-
0*-.
.1--
1-..
-111
--00

Generation = 7, Population = 8.
**.-
-.0.
.*0-
-0..
-**-
----

Generation = 8, Population = 12.
.*.-
0.1.
.*-1
1-..
1**1
-00-

Generation = 9, Population = 8.
.*.-
-.-.
**0-
*0.*
-..*
----

Generation = 10, Population = 9.
...-
0.1.
*.1-
*-.*
1.*.
---0

*** Life<Cell> 10x6 ***

Generation = 0, Population = 29.
0-0---
-0----
-000--
000--0
0-0-0-
-00-00
-0--00
--0-00
-0-00-
-0-0-0

Generation = 1, Population = 33.
-0-0--
-100--
-11100
-11---
1---10
----1-
010011
-0-01-
010--0
01-10-

Generation = 2, Population = 32.
01010-
0*11-0
0*-*-1
------
--00-1
-00--0
1*-1-*
0----0
--100-
--0*1-

Generation = 3, Population = 29.
-*1*1-
1.**01
-.1*-*
011---
---1-*
---01-
-.0--*
1-0--1
01-1--
-1-.-0

Generation = 4, Population = 28.
-**.--
-...1-
-.*.0*
1--0--
10-*1.
----*0
1*1--*
*01-1*
--1-0-
-*0.1-

Generation = 5, Population = 27.
0...1-
-**.*-
0...-*
*--100
-1-*-*
--0-.-
-.-11.
*1*--*
01----
0*-.*0

Generation = 6, Population = 34.
-*.**0
1*.**-
1.*.0*
*-1--1
--0*1*
---0.0
1.1**.
***--.
1*--0-
-*0..1

Generation = 7, Population = 19.
-*.*.1
-....1
-.*.-*
.---0-
11-.-*
0-0-.1
*.-.*.
...--.
-.11--
-*1..-

Generation = 8, Population = 25.
0...**
-.*..-
-...-*
.111-1
-*-.-*
-0-0**
.*1...
...01.
-*-*0-
0*-*.-

*** Life<Cell> 10x3 ***

Generation = 0, Population = 10.
---
--0
--0
--0
0--
0--
---
00-
-0-
0-0

Generation = 1, Population = 18.
--0
-01
-0-
001
100
10-
-0-
1-0
01-
-0-

*** Life<Cell> 10x1 ***

Generation = 0, Population = 6.
-
0
0
0
0
0
-
0
-
-

Generation = 1, Population = 4.
0
1
-
-
-
1
-
-
0
-

Generation = 2, Population = 7.
1
*
0
-
0
-
0
0
-
0

Generation = 3, Population = 5.
*
*
1
-
-
-
1
1
-
-

Generation = 4, Population = 7.
.
*
*
0
-
1
*
*
0
-

Generation = 5, Population = 7.
.
.
*
1
-
*
*
*
1
0

Generation = 6, Population = 4.
.
.
.
*
-
.
*
*
-
1

*** Life<Cell> 1x7 ***

Generation = 0, Population = 5.
000-00-

Generation = 1, Population = 5.
1-1-110

*** Life<Cell> 8x8 ***

Generation = 0, Population = 31.
-0000-00
--0---00
--0-----
0000-00-
0-0--000
-000---0
000----0
-0-0----

Generation = 1, Population = 26.
011-1---
----00--
0----0-0
---1----
1--00-1-
0--100--
1--0--01
-10-0--0

Generation = 2, Population = 38.
1-*-*---
-000--00
-0-0-1--
--0*-000
*001-0-0
-00*1100
*--10-1-
-*10-0-1

Generation = 3, Population = 34.
-1*0.000
--1---11
01---*0-
00-.-1--
*1-*0---
0-1.**1-
*00*--*1
-*--0-0-

Generation = 4, Population = 29.
1**1.11-
00*--0*-
----0*--
110.-*--
.-0*-01-
--*...--
*11.0-.*
-*----1-

Generation = 5, Population = 25.
-..**--0
-1.--1.1
---01.--
*--.-.--
.-1*--*0
0-..**10
**-*-0..
-*--00--

*** Life<Cell> 4x5 ***

Generation = 0, Population = 8.
--0--
000--
-00--
---00

Generation = 1, Population = 11.
0-10-
1110-
----0
-0-11

Generation = 2, Population = 10.
10--0
--*--
0-001
0--*-

Generation = 3, Population = 10.
*1-0-
--*--
1--1*
10-*-

Generation = 4, Population = 10.
.*1-0
--*00
*---*
-1-*-

Generation = 5, Population = 9.
.*-01
1-*-1
.-01*
---.-

Generation = 6, Population = 9.
.*11-
-1.--
.0--*
--0*1

Generation = 7, Population = 10.
.*-*1
1-.01
.1--*
--1*-

Generation = 8, Population = 7.
..-*-
-1.-*
.--1*
--**-

Generation = 9, Population = 10.
..1.-
1-*0*
.1--*
-1**-

Generation = 10, Population = 14.
.**.1
-1*-*
**01*
1-**-

*** Life<Cell> 8x10 ***

Generation = 0, Population = 41.
0-0-------
0-0000000-
-0-0-0-00-
000--00-0-
0----000-0
000-0--00-
00-0--0--0
0-00----0-

Generation = 1, Population = 43.
1-1-00000-
10-1-1-1-0
01-10---10
-11-01--1-
-0----1---
1110--0-10
1--1-0--0-
101-0-00--

Generation = 2, Population = 44.
*0-011-11-
*-0*-*0*-1
-*0--0----
-**0-*-0*-
--000-*0-0
-*--0-1--1
-00*0-0-1-
--*0-011--

*** Life<Cell> 7x3 ***

Generation = 0, Population = 12.
0--
000
--0
00-
0-0
-00
--0

Generation = 1, Population = 11.
1-0
---
-01
-10
1-1
-11
--1

Generation = 2, Population = 7.
---
00-
0--
--1
---
-**
--*

Generation = 3, Population = 13.
10-
-10
1-1
01-
-0-
0**
--*

Generation = 4, Population = 9.
*--
0--
*-*
---
1--
1.*
0-*

Generation = 5, Population = 8.
.0-
-10
.-.
--1
*01
-..
1-.

Generation = 6, Population = 8.
.1-
0-1
.0*
0-*
.--
-..
-0.

Generation = 7, Population = 8.
.--
--*
*1*
-1*
.-1
-..
1-.

Generation = 8, Population = 8.
.-0
0-*
**.
--.
.-*
1..
-0.
